package com.citi.trading.pricing;

import java.util.List;
import java.util.Observer;

public interface PriceObserver extends Observer{
	
	
	public void updatePrice(List<PricePoint> price);
}
